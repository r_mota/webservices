# -*- coding: utf-8 -*-
from django.template.loader import get_template
from django.shortcuts import render_to_response
from django.http import HttpResponse

from bs4 import BeautifulSoup
from StringIO import StringIO
import pycurl
import re
import json
import urllib

def home(request):
	#t = get_template("index.html")
	#tmp = t.render()
	var = "lalala"
	#return HttpResponse(json.dumps(request.GET))
	return render_to_response("index.html",locals())

def schedule(request):
	materias = {}
	get = request.GET
	url = get['url']

	url = urllib.unquote(url.encode('ascii')).decode('utf-8')
	#return HttpResponse(url)

	data = cURL(url)
	data = data.getvalue()	

	data = data.split("<table width=\"480\" border=\"0\" cellpadding=\"3\" cellspacing=\"3\" align=\"center\">")
	data = data[1].split("</table>")

	s = BeautifulSoup(data[0],"html.parser")

	table = s.find_all('tr')
	#print table
	#exit()
	regex = re.compile("<span class=\"style1\"><a id=\"[A-Z]\" name=\"[A-Z]\"></a>")
	rgx = re.compile(".+</b>")

	for i in range(0, (len(table)-1),2):
		mat = {}
		materia = regex.sub("",str(table[i].span))
		materia = materia.replace("<b>  -  <font color=\"#ff0000\"> </font> </b></span>","")
		materia = materia.split("-",1)
		mat['clave'] = materia [0]
		mat['materia'] = materia[1].replace("<b>","")

		adicionales = str(table[i+1]).replace("<td width=\"30\"> </td>","")
		adicionales = adicionales.replace("<td width=\"570\">","")
		adicionales = adicionales.replace("</td>","")
		adicionales = adicionales.replace("<br/>","\n")
		adicionales = adicionales.replace("<tr>","")
		adicionales = adicionales.replace("</tr>","")
		adicionales = adicionales.split("<b>")
		adicionales.pop(0)
		for i in range(0,len(adicionales)):
			adicionales[i] = rgx.sub("", adicionales[i])
			adicionales[i] = adicionales[i].strip()

		mat["profesor"] = adicionales[0]
		mat["horario"] = adicionales[1]
		mat["salon"] = adicionales[2]
		mat["plazas"] = adicionales[3]
		mat["inscritos"] = adicionales[4]
		mat["seccion"] = adicionales[5]
		mat["departamento"] = adicionales[6]
		mat["unidades"] = adicionales[7]
		mat["tipo"] = adicionales[8]
		mat["idioma"] = adicionales[9]
		materias[str(mat["clave"])]=mat

	return HttpResponse(json.dumps(materias))
	#print json.dumps(materias)


def cURL(url):
	URL = str(url)
	#URL = "http://www.udlap.mx/catalogocursos/Cursos.aspx?year=2014&sesion=PRI1"
	content = ""


	data = StringIO()

	curl = pycurl.Curl()
	curl.setopt(pycurl.URL, URL)
	curl.setopt(pycurl.WRITEFUNCTION, data.write)
	curl.perform()
	curl.close()

	return data	